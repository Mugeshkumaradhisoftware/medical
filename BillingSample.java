package medical;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.*;

public class BillingSample {

	public BillingSample() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		JFrame f= new JFrame("Medical Shop Management System");  
		
		JMenu menu,StocksChecking;  
        JMenuItem add_details, delete_details,Update_details,Remove_details;  
        
        
        JMenuBar mb=new JMenuBar();  
        menu=new JMenu("Admin");  
        StocksChecking=new JMenu("Stock"); 
   
        add_details=new JMenuItem("add_details");  
        delete_details=new JMenuItem("delete_details"); 
        
        delete_details.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new  AdminDeleteDetails();
			 }});
        
        Update_details=new JMenuItem("Update_details");
        
        Update_details.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new AdminUpdate();
			 }});
        
        
        add_details.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new Admin();
			 }});
        
         
        menu.add(add_details); 
        menu.add(delete_details); 
        menu.add(Update_details);  
 
        JMenuItem viewstocks=new JMenuItem("view Total Stocks");
        viewstocks.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new P();
			 }});
        StocksChecking.add(viewstocks);
        mb.add(menu);  
        mb.add(StocksChecking);
        f.setJMenuBar(mb);
        
        
        
        
        JTextField SearchBox= new JTextField ();
        JButton Searchbutton=new JButton(" Search Medicine"); 
        SearchBox.setBounds(27,50,900,25);
        Searchbutton.setBounds(1000,50,150,25);
        f.add(SearchBox);
        f.add(Searchbutton);
        
        
        JPanel contentPane =new JPanel(new GridLayout());
        contentPane.setBounds(27,100,900,100);
        contentPane.setBackground(Color.WHITE);
        JTextArea searchResultPane=new JTextArea();
        contentPane.add(searchResultPane);
        
        
        
        
        
        
        int[] count=new  int[1];
        Searchbutton.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 
				 try {
						
						Class.forName("oracle.jdbc.driver.OracleDriver");
						
						String url="jdbc:oracle:thin:@localhost:1521:xe";
						String user="system";
						String password="admin";
								
						Connection con=DriverManager.getConnection(url, user, password);
						Statement stmt=con.createStatement();
						 
						String key=SearchBox.getText();
						ResultSet searchResult=stmt.executeQuery("select count(*) from tabletdetails where tabletname like '"+key+"%'");
	 		 
						 
						int k=0; 
						while(searchResult.next()) { 
							count[k]=searchResult.getInt(1);
						}
						 
						con.close();
				 
					} catch (Exception e1) {
						 
						e1.printStackTrace();
					}
	 			 
			try {
				
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
				String url="jdbc:oracle:thin:@localhost:1521:xe";
				String user="system";
				String password="admin";
						
				Connection con=DriverManager.getConnection(url, user, password);
				Statement stmt=con.createStatement();
				
				
				String key=SearchBox.getText();
				ResultSet searchResult=stmt.executeQuery("select * from tabletdetails where tabletname like '"+key+"%'");
		 		int i=0;
				String[] Search_result_pane=new  String[count[0]];
				 
				
				String[] value = new  String [count[0]];
				while(searchResult.next()) { 
					Search_result_pane[i]=searchResult.getString(2)+"--------------"+searchResult.getInt(3);
					 
					i++;
				}
				String temp="Search Result\n";
			      for(int j=0;j<count[0];j++) {
			    	  temp+=Search_result_pane[j]+"\n";
			    	  
			      } 
			      
			      
	 		      searchResultPane.setText(temp);
	 		      con.close();
				
			} catch (Exception e1) {
				 
				e1.printStackTrace();
			}
			 }});
        
        f.add(contentPane);
		
		
        JPanel BillingPane=new JPanel(new GridLayout());
		BillingPane.setBounds(27,300,900,300);
        BillingPane.setBackground(Color.WHITE);
        JTextArea Bills= new JTextArea ();
        BillingPane.add(Bills);
        
		
		ArrayList <String> listOfTablet=new <String>ArrayList();
		
		 try { 
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
				String url="jdbc:oracle:thin:@localhost:1521:xe";
				String user="system";
				String password="admin";
						
				Connection con=DriverManager.getConnection(url, user, password);
				Statement stmt=con.createStatement();
				 
		 
				ResultSet searchResult=stmt.executeQuery("select tabletname from tabletdetails");
			 
				while(searchResult.next()) {
					listOfTablet.add(searchResult.getString(1));
					 
				}

				con.close();
				
			} catch (Exception e1) {
				 
				e1.printStackTrace();
			}
		
		 
		 
		 
		 
		 String[] ListForComboBox=new String[listOfTablet.size()];   
		
		 
		 for(int i=0;i<listOfTablet.size();i++){
			ListForComboBox[i]=listOfTablet.get(i);
		}
		
		JLabel Total_Label=new JLabel("TOTAL");
		Total_Label.setBounds(800,610,700,25); 
		f.add(Total_Label);
		
		JTextField total= new JTextField ("0");
		total.setBounds(860,610,70,25);
		f.add(total);
		
		JComboBox tabletList=new JComboBox(ListForComboBox);
		
		tabletList.setBounds(27,250,700,25); 
		f.add(tabletList);
		
		JLabel No_of_items_Label=new JLabel("NO OF ITEMS");
		No_of_items_Label.setBounds(750,250,700,25); 
		f.add(No_of_items_Label);

		JTextField No_of_items= new JTextField ("1");
		No_of_items.setBounds(830,250,70,25);
		f.add(No_of_items);
		 
		ArrayList <Integer> listOfItems=new ArrayList();
		
		JButton addToCart=new JButton("Add to cart");
		addToCart.setBounds(1000,250,150,25);
		
		ArrayList billing_items=new ArrayList();
		addToCart.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 
				 try { 
						Class.forName("oracle.jdbc.driver.OracleDriver");
						
						String url="jdbc:oracle:thin:@localhost:1521:xe";
						String user="system";
						String password="admin";
								
						Connection con=DriverManager.getConnection(url, user, password);
						Statement stmt=con.createStatement();
						int no=Integer.parseInt(No_of_items.getText()); 
						String key=(String) tabletList.getSelectedItem();
						ResultSet searchResult=stmt.executeQuery("select tabletprice from tabletdetails where tabletname='"+key+"'");
					 
						while(searchResult.next()) {
							 System.out.println("-------------->"+searchResult.getInt(1));
							 listOfItems.add(no*searchResult.getInt(1));
						}

						con.close();
						
					} catch (Exception e1) {
						 
						e1.printStackTrace();
					}
	 
				 int temp=0;
				 for(int i=0;i<listOfItems.size();i++) {
					 temp+=listOfItems.get(i); 
				 }
				 total.setText(temp+"");
				 
				 
				 int row=listOfItems.size();
				 System.out.println("row----->"+row);
				 ArrayList <String> [] Stocks=new ArrayList[row];
				    
				    for (int i = 0; i <row; i++) { 
				    	Stocks[i] = new ArrayList<String>(); 
			        }  
				 try { 
						Class.forName("oracle.jdbc.driver.OracleDriver");
						
						String url="jdbc:oracle:thin:@localhost:1521:xe";
						String user="system";
						String password="admin";
								
						Connection con=DriverManager.getConnection(url, user, password);
						Statement stmt=con.createStatement();
						
						
						int no=Integer.parseInt(No_of_items.getText()); 
						String key=(String) tabletList.getSelectedItem();
						ResultSet searchResult=stmt.executeQuery("select * from tabletdetails where tabletname='"+key+"'");
						
						
						int j=0;
						
						while(searchResult.next()) {
						 
							int k= searchResult.getInt(3);
							int price=no*k;
							billing_items.add(j+"\t\t              "+ searchResult.getString(2)+"\t\t"+no+"\t\t\t"+price);
//							Stocks[i].add(""+i+1);
////							System.out.println(Stocks[i]);
//							Stocks[i].add( searchResult.getString(2));
////							System.out.println(Stocks[i]);
//							
//							Stocks[i].add(""+no);
////							System.out.println(Stocks[i]);
////							int values=no*(Integer.toString(searchResult.getInt(3));
//							Stocks[i].add(Integer.toString(searchResult.getInt(3)));
////							System.out.println(Stocks[i]);
							j++;
							  
						}

						con.close();
						
					} catch (Exception e1) {
						 
						e1.printStackTrace();
					}
				 String tempVar="";
				 for(int k=0;k<billing_items.size();k++) {
//				 System.out.println(billing_items.get(k));
				 tempVar+=billing_items.get(k)+"\r\n";

			 		}
				 Bills.setText(tempVar);
				 
				 
//				 int row1=row;
////				 String data[][]=new String[row1][4];
//////				    
////				    for(int i=0;i<data.length;i++) {
////			       	 
////			        	for(int j=0;j<data[i].length;j++) {
////			        		data[i][j]=Stocks[i].get(j);
//////			        		System.out.println(data[i][j]);
////			        		
////			        	}
////			        }
//				 
//				  
//				 String data[][]= {{"1","2","3","4"}};
//			        String column[]={"No","Tablet Name","Tablet Quantity","price"};         
//				    
//				    JTable jt=new JTable(data,column);    
				      
//				    BillingPane.add(jt);; 
				    
	 				 
			 }});
		
		
		f.add(addToCart);
		
      f.add(BillingPane);
        JLabel NO=new JLabel("NO");
        NO.setBounds(25,280,100,25); 
		 f.add(NO);
        
		 JLabel Tablet_Name=new JLabel("Tablet Name");
		 Tablet_Name.setBounds(245,280,100,25); 
		 f.add(Tablet_Name);
		 
		 JLabel Quantity=new JLabel("Quantity");
		 Quantity.setBounds(465,280,100,25); 
		 f.add(Quantity);
		 
		 
		 JLabel Price=new JLabel("Price");
		 Price.setBounds(725,280,100,25); 
		 f.add(Price);
		 
		 
		 JButton Print =new JButton("Print");
	     Print.setBounds(1170,610,150,25);
	     Print.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
	     
	     try {
	            FileWriter writer = new FileWriter("C:\\Users\\MADHAN\\Desktop\\billing.txt", true);
	            writer.write("\n------------------Hello to Medical Shop---------------------------------------------");
	            writer.write("\r\n----------------------------------------------------------------------------------");   // write new line
	            writer.write("\r\n-----------------------------------------billing-----------------------------------");
	            writer.write("\r\n NO                        Tablet name        Quantity                price");
	            writer.write("\r\n-----------------------------------------------------------------------------------");   // write new line
	            writer.write("\r\n");
	            writer.write(Bills.getText());
	            writer.write("\r\n------------------------------------------------------------------------------------");   // write new line
	            writer.write("\r\n");
	            writer.write("                                                		Total	"+total.getText());
	            writer.write("\r\n------------------------------------------------------------------------------------");   // write new line

	            writer.close();
	        } catch (IOException e1) {
	            e1.printStackTrace();
	        }
			 }});
		 f.add(Print);
		 
		 JLabel removeLabel=new JLabel("[ *Nth Items to Remove ]");
		 removeLabel.setBounds(1170,330,200,25);
		 f.add(removeLabel);
		 JTextField removeBox=new JTextField();
		 removeBox.setBounds(1170,300,50,25);
		 f.add(removeBox);
		 JButton removeItems=new JButton("Remove");
		 removeItems.setBounds(1000,300,150,25);
		 removeItems.addActionListener(new ActionListener()
				 {

					 
					public void actionPerformed(ActionEvent arg0) {
						 int index1=Integer.parseInt(removeBox.getText());
						 int index=index1-1;
				 
						 billing_items.remove(index);
						 listOfItems.remove(index);
						 
						 String tempVar="";
						 for(int k=0;k<billing_items.size();k++) {
//						 System.out.println(billing_items.get(k));
						 tempVar+=billing_items.get(k)+"\r\n";

					 		}
						 Bills.setText(tempVar);
						 int temp=0;
						 for(int i=0;i<listOfItems.size();i++) {
							 temp+=listOfItems.get(i); 
						 }
						 total.setText(temp+"");
					}
			 
				 });
		 
		 
		 f.add(removeItems);
		 
		 
		 
		 
		f.setSize(1366,768);  
        f.setLayout(null);  
        f.setVisible(true); 
	}

}
